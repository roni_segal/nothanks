package com.server;

import com.server.Tools.Code;
import com.server.Tools.IHandleMessage;
import com.server.Tools.Log;
import com.server.Tools.Message;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import static java.lang.System.exit;

/**
 * Created by ronio on 6/29/2017.
 */
public class ClientHandler {
    public static final char SPLITER = '|';
    private boolean isLogedin;
    public int clientID;


    enum State{notLogedin}
    private Socket clientSocket;
    private BufferedReader in;
    private DataOutputStream out;
    private State playerState;
    private int databaseID;
    private boolean isConnected;

    public static IHandleMessage[] methodPointers = new IHandleMessage[]
            {
                    new Login(),
                    new Register()
            };



    private Thread loopThread;
    public ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
        playerState = State.notLogedin;
        databaseID = -1;
        isLogedin = false;
        isConnected = true;

        try {
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
        } catch (IOException e) {
            Log.E("Could not get out stream or in stream for the client");
            System.exit(1);
        }

        loopThread = new Thread(this::loop);
        loopThread.start();
    }

    private void loop() {
        while(true) {
            Message msg = Read();
            if(msg == null)
                break;
            methodPointers[msg.getCode()].Handle(this, msg.getArgs());
        }
    }

    private Message Read() {
        try {
            int val;
            String res = "";
            do {
                val = in.read();
                res += (char)val;
            }while(val != Code.END_MESSAGE && val != -1);
            Log.D("client ", clientID, "Read:", res);
            return new Message(res);
        } catch (IOException e) {
            Log.E("Could not read from the server, disconnecting");
            isConnected = false;
            return null;
        }
    }

    public void Write(Message msg) {
        try {
            out.writeBytes(msg.toString());
            out.flush();
            Log.D("client ", clientID, "Write:", msg.toString());
        } catch (IOException e) {
            Log.E("Could not send message to the client");
            exit(1);
        }
    }


    public static class Login implements IHandleMessage {
        @Override
        public void Handle(ClientHandler client, String[] args) {
            if (Database.Login(args[0], args[1])) {
                client.isLogedin = true;
                client.Write(new Message(Code.LOGIN, Code.TRUE));
            }else client.Write(new Message(Code.LOGIN, Code.FALSE));
        }
    }

    public static class Register implements  IHandleMessage {
        @Override
        public void Handle(ClientHandler client, String[] args) {
            if (Database.Register(args[0], args[1])) {
                client.isLogedin = true;
                client.Write(new Message(Code.REGISTER, Code.TRUE));
            }else client.Write(new Message(Code.REGISTER, Code.FALSE));
        }
    }
}
