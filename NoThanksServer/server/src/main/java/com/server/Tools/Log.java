package com.server.Tools;

/**
 * Created by ronio on 6/29/2017.
 */

public class Log {
    public static void E(Object... msgs) {
        System.out.print("E >> ");
        for(int i = 0; i < msgs.length; i++) {
            System.out.print(msgs[i] + " ");
        }
        System.out.println();
    }

    public static void D(Object... msgs) {
        System.out.print("D >> ");
        for(int i = 0; i < msgs.length; i++) {
            System.out.print(msgs[i] + " ");
        }
        System.out.println();
    }

    public static void I(Object... msgs) {
        System.out.print("I >> ");
        for(int i = 0; i < msgs.length; i++) {
            System.out.print(msgs[i] + " ");
        }
        System.out.println();
    }
}