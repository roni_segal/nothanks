package com.server.Tools;

import com.server.ClientHandler;

/**
 * Created by ronio on 6/29/2017.
 */

public interface IHandleMessage {
    public void Handle(ClientHandler client, String[]args);
}
