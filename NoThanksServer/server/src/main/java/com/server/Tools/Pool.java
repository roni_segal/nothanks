package com.server.Tools;

/**
 * Created by ronio on 6/29/2017.
 */

public class Pool<E> {
    public static int CAPACITY = 20;
    E[] arr;
    int num;

    public Pool() {
        arr = (E[])new Object[CAPACITY];
        num = 0;
    }

    public int insert(E val) {
        // TODO: resize
        if(num == arr.length) {
            E[] arr2 = (E[])new Object[arr.length + CAPACITY];
            for(int i = 0; i < arr.length; i++) {
                arr2[i] = arr[i];
            }
            arr = arr2;
        }

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] == null) {
                arr[i] = val;
                num++;
                return i;
            }
        }
        return -1;
    }
}
