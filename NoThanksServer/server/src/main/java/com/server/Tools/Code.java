package com.server.Tools;

/**
 * Created by ronio on 6/29/2017.
 */

public class Code {
    public static final String TRUE = "1";
    public static final String FALSE = "0";
    public static final char END_MESSAGE = '%';
    public static final char SPLITER = '|';

    public static final Byte LOGIN = new Byte("0");
    public static final Byte REGISTER = new Byte("1");
}
