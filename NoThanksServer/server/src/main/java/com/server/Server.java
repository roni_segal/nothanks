package com.server;

import com.server.Tools.Log;
import com.server.Tools.Pool;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class Server {
    public static ServerSocket serverSocket;
    public static Pool<ClientHandler> clientHandlers;

    public static void main (String[] arg){
        serverSocket = null;
        clientHandlers = new Pool<>();
        Database.Init();
        //DatabaseHendler.Init();
        try {
            serverSocket = new ServerSocket(4444);
        } catch (IOException e) {
            Log.E("Could not listen on port: 4444.");
            System.exit(1);
        }

        Socket clientSocket = null;
        Log.I("server running");
        while(true) {
            try {
                clientSocket = serverSocket.accept();
                Log.I("client connected");
                ClientHandler client = new ClientHandler(clientSocket);
                client.clientID = clientHandlers.insert(client);

            } catch (IOException e) {
                Log.E("Accept failed.");
                System.exit(1);
            }
        }    }
}
