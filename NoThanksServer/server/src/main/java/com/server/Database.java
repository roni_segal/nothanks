package com.server;

import com.server.Tools.Log;
import com.sun.org.apache.xml.internal.dtm.DTMAxisIterator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by ronio on 6/29/2017.
 */

public class Database {
    private static Connection connection;
    private static Statement statement;

    private Database() {}

    public static void Init() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:database/database.db");
            statement = connection.createStatement();
            //ExecuteUpdate("PRAGMA foreign_keys = ON;");
        } catch ( Exception e ) {
            Log.E("error Database:Init " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
        }
    }

    private static ResultSet ExecuteQuery(String query) {
        Log.I("ExecuteQuery: " + query);
        try {
            return statement.executeQuery(query);
        } catch (SQLException e) {
            Log.E("error Database:ExecuteQuery " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            return null;
        }
    }
    private static int ExecuteUpdate(String query) {
        Log.I("ExecuteUpdate: " + query);
        try {
            return statement.executeUpdate(query);
        } catch (SQLException e) {
            Log.E("error Database:ExecuteUpdate " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            return 0;
        }
    }

    public static boolean Register(String username, String password) {
        return ExecuteUpdate("INSERT INTO users(username, password) VALUES(\""+ username +"\", \""+ password +"\")") == 1;
    }

    public static boolean Login(String username, String password) {
        try {
            return  ExecuteQuery("SELECT * FROM users WHERE username=\"" + username + "\" and password=\"" + password + "\"").next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
