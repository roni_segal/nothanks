package client2.Tools;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import static java.lang.System.exit;

/**
 * Created by ronio on 8/10/2017.
 */


public class Client {
    private static IClientCallback mainCallbackPointer; // data of all the screens
    private static IClientCallback callbackPointer; // where to put recved messages
    private static Socket socket;
    private static BufferedReader in;
    private static DataOutputStream out;

    private static Thread recvThread;

    // connect to the server
    public static void Connect(String ip, int port) {
        try {
            socket = new Socket(ip, port);
        } catch (IOException e) {
            Log.E("could not connect to the server");
            exit(1);
        }

        try {
            out = new DataOutputStream(socket.getOutputStream());
            in = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
        } catch (IOException e) {
            Log.E("could not get out steam or in steam");
        }

        recvThread = new Thread(Client::loop);
        recvThread.start();
    }


    // recving loop
    private static void loop() {

        while(true) {
            Message msg = Read();
            if(mainCallbackPointer != null) {
                mainCallbackPointer.Callback(msg);
            }else {
                Log.E("mainCallbackPointer is null, msg not handheld");
            }
            if(callbackPointer != null) {
                callbackPointer.Callback(msg);
            }else {
                Log.E("callbackPointer is null, msg not handheld");
            }
        }
    }

    // set a callback to the messages
    public static void setCallback(IClientCallback callbackPointer) {
        Client.callbackPointer = callbackPointer;
    }

    public static void setMainCallbackPointer(IClientCallback mainCallbackPointer) {
        Client.mainCallbackPointer = mainCallbackPointer;
    }

    // read a message from the server (will wait until a message is recved
    private static Message Read() {
        try {
            int val;
            String res = "";
            do {
                val = in.read();
                res += (char)val;
            }while(val != Code.END_MESSAGE && val != -1);
            Message resMsg = new Message(res);
            Log.D("Read:", resMsg.print());
            return resMsg;
        } catch (IOException e) {
            Log.E("Could not read from the server, disconnecting");
            return null;
        }
    }

    // Write a message to the server
    public static void Write(Message msg) {
        try {
            Log.D("Write:", msg.print());
            out.writeBytes(msg.toString());
            out.flush();
        } catch (IOException e) {
            Log.E("Could not send message to the client");
            exit(1);
        }
    }
}
