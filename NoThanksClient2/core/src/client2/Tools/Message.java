package client2.Tools;

/**
 * Created by ronio on 8/10/2017.
 */
public class Message {

    private Byte code; // the code at the start of the messsage
    private String[] args; // args to pass in the message
    private byte[] bytes; // the all message in bytes preperd to send

    public Message(Byte code, Object... args) {
        String argsToSend = "";
        this.code = code;
        this.args = new String[args.length];
        for(int i = 0; i < args.length; i++) {
            argsToSend += args[i].toString() + Code.SPLITER;
            this.args[i] = args[i].toString();
        }
        if(argsToSend.length() != 0) {
            argsToSend = argsToSend.substring(0, argsToSend.length() - 1); // remove the last spliter
        }

        bytes = new byte[argsToSend.length() + 2];
        bytes[0] = code;
        for(int i = 1; i <= argsToSend.length(); i++) {
            bytes[i] = (byte) argsToSend.charAt(i - 1);
        }
        bytes[bytes.length - 1] = Code.END_MESSAGE;
    }

    public Message(byte[] msg) {
        code = msg[0];
        String strMsg = "";
        for(int i = 1; i < msg.length; i++) {
            strMsg += (char)msg[i];
        }
        strMsg = strMsg.substring(0, strMsg.length() - 1);
        args = strMsg.split("\\" + Code.SPLITER);
    }

    public Message(String res) {
        this(res.getBytes());
    }

    public byte[] getBytes() {
        return bytes;
    }

    public String toString() {
        return new String(bytes);
    }

    public Byte getCode() {
        return code;
    }

    public String[] getArgs() {
        return args;
    }

    public String print() {
        String strArgs = "";
        for(int i = 0; i < getArgs().length; i++)
            strArgs += getArgs()[i] + " | ";
        return getCode() + " " + strArgs;
    }
}