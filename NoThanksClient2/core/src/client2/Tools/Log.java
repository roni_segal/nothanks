package client2.Tools;

/**
 * Created by ronio on 8/10/2017.
 */


public class Log {
    // print logs
    public static void E(Object... msgs) {
        System.out.print("E >> ");
        print(msgs);
        System.out.println();
    }

    public static void D(Object... msgs) {
        System.out.print("D >> ");
        print(msgs);
        System.out.println();
    }

    public static void I(Object... msgs) {
        System.out.print("I >> ");
        print(msgs);
        System.out.println();
    }

    private static void print(Object... msgs) {
        for(int i = 0; i < msgs.length; i++) {
            System.out.print(msgs[i] + " ");
        }
    }
}