package client2.Tools;

/**
 * Created by ronio on 8/10/2017.
 * sends and recv messages from the server
 */

public class Code {
    // server and client protocol data
    public static final String TRUE = "1";
    public static final String FALSE = "0";
    public static final char SPLITER = '|';
    public static final char END_MESSAGE = '%';

    public static final Byte LOGIN = new Byte("0");
    public static final Byte REGISTER = new Byte("1");
    public static final Byte GET_USERNAME = new Byte("2");
    public static final Byte GET_LEVEL = new Byte("3");
    public static final Byte GET_CASH = new Byte("4");
    public static final Byte GET_TANK = new Byte("5");

    public static final Byte GET_BODYS = new Byte("6");
    public static final Byte GET_BARRLES = new Byte("7");

    public static final Byte REQUEST = new Byte("23");

}
