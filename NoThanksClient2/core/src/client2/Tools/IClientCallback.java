package client2.Tools;

/**
 * Created by ronio on 8/10/2017.
 */

// classes that want to listen to the server need to implement this
public interface IClientCallback {
    void Callback(Message msg);
}
