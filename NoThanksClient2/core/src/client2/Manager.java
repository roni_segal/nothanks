package client2;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;

import client2.Screens.GameScreen;
import client2.Screens.LoginScreen;
import client2.Tools.Client;
import client2.Tools.Code;
import client2.Tools.IClientCallback;
import client2.Tools.Message;

public class Manager extends Game implements IClientCallback {
	public static final float V_WIDTH = 800;
	public static final float V_HEIGHT = (V_WIDTH / (4f/3f));
	//public static final float V_HEIGHT = (int) (V_WIDTH / (16f/9f));

	public static final String ip = "127.0.0.1";
	public static final int port = 4444;

	private static Skin skin;


	public static Batch batch;
	public static ShapeRenderer sr;

	private static Manager instance;

	@Override
	public void create() {
		Client.Connect(ip, port);
		Client.setMainCallbackPointer(this);
		batch = new SpriteBatch();
		sr = new ShapeRenderer();
		instance = this;

		GameScreen.cam = new OrthographicCamera();
		GameScreen.viewport = new FitViewport(Manager.V_WIDTH, Manager.V_HEIGHT, GameScreen.cam);
		skin = new Skin(Manager.getInternalFile("ui4/uiskin.json"));

		setGameScreen(new LoginScreen());
		Client.Write(new Message(Code.LOGIN, "nhrnhr0", "6464f8@@")); // skips login
	}

	// every time a screen is switched: set input processor to the next screen and set the client callback
	public static void setGameScreen(GameScreen screen) {
		Client.setCallback(screen);
		Gdx.input.setInputProcessor(screen);
		instance.setScreen(screen);
	}

	@Override
	public void Callback(Message msg) {
		//TODO: could use to recv files
	}

	// gets the file, if not found request from the server
	public static FileHandle getInternalFile(String filePath) {
		return Gdx.files.internal(filePath);
	}

	// gets the texture, if not found requests from the server
	public static String LoadFile(String filePath) {
		return filePath;
	}

	public static Skin getSkin() {
		return skin;
	}
}