package client2.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import client2.Manager;
import client2.Tools.Client;
import client2.Tools.Code;
import client2.Tools.Message;

/**
 * Created by ronio on 8/10/2017.
 */

public class LoginScreen extends GameScreen {
    private Stage backgroundStage;
    private Stage stage;
    private InputMultiplexer myInputProcessor;

    private Table table;

    private TextField username;
    private TextField password;
    private ImageButton loginBtn;
    private ImageButton registerBtn;
    private Texture bgTexture;
    private boolean moveToNextScreen = false;


    public LoginScreen() {
        backgroundStage = new Stage(viewport, Manager.batch);
        stage           = new Stage(viewport, Manager.batch);

        Image bg = new Image(bgTexture = new Texture(Gdx.files.internal("bg.png")));
        bg.setFillParent(true);
        backgroundStage.addActor(bg);

        table = new Table();
        table.setFillParent(true);
        table.add(new Label("name: ", Manager.getSkin()));
        table.add(username = new TextField("", Manager.getSkin())).width(225); // TODO: enter will move to pass field
        table.row();
        table.add(new Label("password: ", Manager.getSkin()));
        table.add(password = new TextField("", Manager.getSkin())).width(225); // TODO: enter will click and tab will move to button
        table.row();
        table.add(loginBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Manager.LoadFile("login.png")))),
                new TextureRegionDrawable(new TextureRegion(new Texture(Manager.LoadFile("loginPressed.png")))))).size(150, 100);
        table.add(registerBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(Manager.LoadFile("register.png")))),
                new TextureRegionDrawable(new TextureRegion(new Texture(Manager.LoadFile("registerPressed.png")))))).size(150, 100);
        table.row();
        loginBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Client.Write(new Message(Code.LOGIN, username.getText(), password.getText()));
            }
        });
        registerBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Client.Write(new Message(Code.REGISTER, username.getText(), password.getText()));
            }
        });
        stage.addActor(table);
        stage.setKeyboardFocus(username);
        myInputProcessor = new InputMultiplexer();

    }


    @Override
    public void show() {
        myInputProcessor.addProcessor(this);
        myInputProcessor.addProcessor(stage);
        Gdx.input.setInputProcessor(myInputProcessor);
    }

    @Override
    protected void update(float dt) {
        backgroundStage.act();
        stage.act();

        if(moveToNextScreen) {
            Manager.setGameScreen(new MainScreen());
        }
    }

    @Override
    public void Callback(Message msg) {
        if(msg.getCode().equals(Code.LOGIN) || msg.getCode().equals(Code.REGISTER)) {
            if(msg.getArgs()[0].equals(Code.TRUE))
                moveToNextScreen = true;
            else {
                Gdx.app.postRunnable(() -> {
                    Dialog d = new Dialog("", Manager.getSkin(), "dialog");
                    d.getBackground().setMinWidth(0);
                    d.getBackground().setMinHeight(150);
                    d.text("wrong  username or password");
                    d.addListener(new InputListener() {
                        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                            d.hide();
                            return false;
                        }
                    });
                    d.show(stage);
                });
            }
        }
    }

    @Override
    protected void postDraw(ShapeRenderer sr) {
    }

    @Override
    protected void draw(Batch batch) {
        // clear screen
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(cam.combined);
        backgroundStage.draw();
        stage.draw();
    }

    @Override
    protected void preDraw(ShapeRenderer sr) {

    }

    @Override
    public void dispose() {
        bgTexture.dispose();
        //TODO: dispose this loginBtn.getImage()
        //TODO: dispose this registerBtn.getImage()
        backgroundStage.dispose();
        stage.dispose();
    }
}
