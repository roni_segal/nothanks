package client2.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;

import client2.Manager;
import client2.Tools.Client;
import client2.Tools.Code;
import client2.Tools.Log;
import client2.Tools.Message;
import client2.assets.TankUI;

/**
 * Created by ronio on 8/10/2017.
 */

public class MainScreen extends GameScreen {

    private Stage backgroundStage;
    private Stage stage;
    private Texture bgTexture;
    private Table tableMain;

    private Table tableUserData;
    private Table tableMyTanks;
    private Table tableShop;
    private Table tableUsers;

    // tableUserData elements:
    private Label lblUsername1;
    private Label lblLevel1;
    private Label lblCash1;
    private Label lblUsername2;
    private Label lblLevel2;
    private Label lblCash2;

    // tableMyTanks:
    private int myTanksSize = 8;
    private TankUI[] myTanks;
    private InputMultiplexer myInputProcessor;


    public MainScreen() {
        backgroundStage = new Stage(viewport, Manager.batch);
        stage = new Stage(viewport, Manager.batch);

        Image bg = new Image(bgTexture = new Texture(Gdx.files.internal("bg.png")));
        bg.setFillParent(true);
        backgroundStage.addActor(bg);

        tableMain = new Table(Manager.getSkin());
        stage.addActor(tableMain);
        tableMain.debug();
        tableMain.setFillParent(true);
        tableMain.add(tableMyTanks = new Table(Manager.getSkin()));
        tableMain.add(tableUserData = new Table(Manager.getSkin()));
        tableMain.row();
        tableMain.add(tableShop = new Table(Manager.getSkin()));
        tableMain.add(tableUsers = new Table(Manager.getSkin()));

        // set tableUserData
        tableUserData.add(lblUsername1 = new Label("username: ", Manager.getSkin()));
        tableUserData.add(lblUsername2 = new Label("", Manager.getSkin()));
        tableUserData.row();
        tableUserData.add(lblLevel1 = new Label("level: ", Manager.getSkin()));
        tableUserData.add(lblLevel2 = new Label("", Manager.getSkin()));
        tableUserData.row();
        tableUserData.add(lblCash1 = new Label("cash: ", Manager.getSkin()));
        tableUserData.add(lblCash2 = new Label("", Manager.getSkin()));
        tableUserData.row();

        // set tableMyTanks
        myTanksSize = (myTanksSize / 2) * 2; // make sure myTankSize is even
        myTanks = new TankUI[myTanksSize];
        for (int i = 0; i < myTanks.length / 2; i++) {
            myTanks[i] = new TankUI(i);
            tableMyTanks.add(myTanks[i]);
        }
        tableMyTanks.row();
        for (int i = 0; i < myTanks.length / 2; i++) {
            myTanks[i + myTanks.length / 2] = new TankUI(i + myTanks.length / 2);
            tableMyTanks.add(myTanks[i + myTanks.length / 2]);
        }


        DragAndDrop dnd = new DragAndDrop();
        dnd.addSource(new DragAndDrop.Source(tableMyTanks) {
            final DragAndDrop.Payload payload = new DragAndDrop.Payload();
            int dragedIndex;
            @Override
            public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
                for(int i = 0; i < myTanks.length; i++) {
                    if(myTanks[i].getBounds().contains(x,y)) {
                        System.out.println("selected tank index: " + i);
                        dragedIndex = i;
                        payload.setObject(myTanks[i]);
                        myTanks[i].startDrag();
                        payload.setDragActor(myTanks[i].getDragedImage());
                        //dnd.setDragActorPosition(-x + myTanks[i].getWidth() * 2, -y + myTanks[i].getHeight());
                        dnd.setDragActorPosition(myTanks[i].getWidth() / 4, -myTanks[i].getHeight() / 4);
                        return payload;
                    }
                }
                dragedIndex = -1;
                return null;
            }

            public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
                if(target == null) {
                    myTanks[dragedIndex].stopDrag();
                    dragedIndex = -1;
                }
            }
        });
        dnd.addTarget(new DragAndDrop.Target(tableMyTanks) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                return true;
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                for(int i = 0; i < myTanks.length; i++) {
                    if(myTanks[i].getBounds().contains(x,y)) {
                        if(myTanks[i].canDroped(payload.getObject())) {
                            myTanks[i].drop(payload.getObject());
                        } else {
                            ((TankUI)payload.getObject()).drop(payload.getObject());
                        }
                        break;
                    }
                }
            }
        });
    }


    @Override
    public void show() {
        myInputProcessor = new InputMultiplexer();
        myInputProcessor.addProcessor(this);
        myInputProcessor.addProcessor(stage);
        Gdx.input.setInputProcessor(myInputProcessor);


        Client.Write(new Message(Code.GET_USERNAME));
        Client.Write(new Message(Code.GET_LEVEL));
        Client.Write(new Message(Code.GET_CASH));
        for(int i = 0; i <myTanks.length; i++) {
            Client.Write(new Message(Code.GET_TANK, i));
        }
        Log.D("done sending requests");
    }

    @Override
    protected void update(float dt) {
        backgroundStage.act();
        stage.act();
    }

    @Override
    protected void draw(Batch batch) {
        // clear screen
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        backgroundStage.draw();
        stage.draw();
    }



    @Override
    public void Callback(Message msg) {
        if(msg.getCode().equals(Code.GET_USERNAME))
            Gdx.app.postRunnable(() -> {
             lblUsername2.setText(msg.getArgs()[0]);
            });
        else if(msg.getCode().equals(Code.GET_LEVEL))
            Gdx.app.postRunnable(() -> {
                lblLevel2.setText(msg.getArgs()[0]);
            });
        else if(msg.getCode().equals(Code.GET_CASH))
            Gdx.app.postRunnable(() -> {
                lblCash2.setText(msg.getArgs()[0]);
            });
        else if(msg.getCode().equals(Code.GET_TANK)) {
            int tankIndex = Integer.parseInt(msg.getArgs()[0]);
            Gdx.app.postRunnable(() -> {
                myTanks[tankIndex].setBodyImage(msg.getArgs()[1] + ".png");
                myTanks[tankIndex].setBarrelImage(msg.getArgs()[2] + ".png");
            });
        }

    }

    @Override
    protected void preDraw(ShapeRenderer sr) {

    }

    @Override
    protected void postDraw(ShapeRenderer sr) {
    }

    @Override
    public void dispose() {
        bgTexture.dispose();
        backgroundStage.dispose();
        stage.dispose();
    }

    public static Vector2 getStageLocation(Actor actor) {
        return actor.localToStageCoordinates(new Vector2(0, 0));
    }
}
