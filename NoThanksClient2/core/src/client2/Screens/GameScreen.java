package client2.Screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;

import client2.Manager;
import client2.Tools.IClientCallback;
import client2.Tools.Message;

/**
 * Created by ronio on 8/10/2017.
 */

public abstract class GameScreen implements InputProcessor, Screen, IClientCallback {
    public static FitViewport viewport;
    public static OrthographicCamera cam;

    @Override
    public abstract void show();
    protected abstract void update(float dt);
    protected abstract void draw(Batch batch);
    @Override
    public abstract void Callback(Message msg);

    protected abstract void preDraw(ShapeRenderer sr);
    protected abstract void postDraw(ShapeRenderer sr);

    @Override
    public abstract void dispose();


    @Override
    public void render(float delta) {
        update(delta);
        preDraw(Manager.sr);
        draw(Manager.batch);
        postDraw(Manager.sr);
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}