package client2.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by ronio on 8/12/2017.
 */


public class TankUI extends Image {
    enum part {BODY,BARRLE, NULL}
    //private Image background;
    private Image bodyImage;
    private Image barrelImage;
    private Image dragImageBackup;
    private int index = -1;
    private part dragedPart;

    public TankUI(int index) {
        super(new TextureRegion(new Texture("tankBackground2.png")));
        /*
        bodyImage = new Image(new Texture("HavyBody1.png"));
        barrelImage = new Image(new Texture("HavyBarrle1.png"));
        */
        this.index = index;
        dragedPart = part.NULL;
        //setBounds(getX(), getY(), getWidth(), getHeight());
        //setBounds(0,0,800,800);
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setBodyImage(String body) {
        if(!body.equals("-1.png")) {
            bodyImage = new Image(new Texture(body));
            bodyImage.setName(body);
        }
        else // TODO: dispose old image
            bodyImage = null;

    }
    public void setBarrelImage(String barrel) {
        if(!barrel.equals("-1.png")) {
            barrelImage = new Image(new Texture(barrel));
            barrelImage.setName(barrel);
        }
        else // TODO: dispose old image
            barrelImage = null;
    }

    private void setBodyImage(Image bodyImageArg) {
        if(bodyImageArg == null) {
            bodyImage =  null;
        }
        else {
            bodyImage = bodyImageArg;
            bodyImage.setName(bodyImageArg.getName());
        }
    }

    private void setBarrelImage(Image barrelImageArg) {
        if(barrelImageArg == null) {
            barrelImage = null;
        }
        else {
            barrelImage = barrelImageArg;
            barrelImage.setName(barrelImageArg.getName());
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(bodyImage != null) {
            bodyImage.setX(getX() + ((getWidth() - bodyImage.getWidth()) / 2));
            bodyImage.setY(getY() + ((getHeight() - bodyImage.getHeight()) / 2));
            bodyImage.draw(batch, parentAlpha);
        }

        if(barrelImage != null) {
            barrelImage.setX(getX() + ((getWidth() - barrelImage.getWidth()) / 2));
            barrelImage.setY(getY() + ((getHeight() - barrelImage.getHeight()) / 2));
            barrelImage.draw(batch, parentAlpha);
        }
    }

    public Rectangle getBounds() {
        return new Rectangle(
                getX(), getY(),
                getWidth(), getHeight());
    }


    public void startDrag() {
        if(barrelImage != null) {
            dragImageBackup = barrelImage;
            dragedPart = part.BARRLE;
            setBarrelImage((Image)null); //barrelImage = null;

        }
        else if(bodyImage != null) {
            dragImageBackup = bodyImage;
            dragedPart = part.BODY;
            setBodyImage((Image)null); //bodyImage = null;

        }
    }

    public Actor getDragedImage() {
        return dragImageBackup;
    }

    public void stopDrag() {
        if(isDragingBarrle()) {
            dragedPart = part.NULL;
            setBarrelImage(dragImageBackup); //barrelImage = dragImageBackup;
            dragImageBackup = null;
        }
        else if(isDragingBody()) {
            dragedPart = part.NULL;
            setBodyImage(dragImageBackup); //bodyImage = dragImageBackup;
            dragImageBackup = null;
        }
    }

    public boolean canDroped(Object object) {
        if(!(object instanceof TankUI))
            return false;
        TankUI draged = (TankUI)object;
        if(barrelImage == null && bodyImage == null) {
            return true;
        }

        if(bodyImage == null &&
                draged.isDragingBody()) {
            return true;
        }

        if(barrelImage == null &&
                draged.isDragingBarrle()) {
            return true;
        }

        return false;
    }


    public part getDragedPart() {
        return dragedPart;
    }

    public void drop(Object object) {
        if(!(object instanceof TankUI))
            return;
        TankUI draged = (TankUI)object;
        if(draged.getDragedPart() == part.BARRLE) {
            barrelImage = draged.dragImageBackup;
        }
        else if(draged.getDragedPart() == part.BODY) {
            bodyImage = draged.dragImageBackup;
        }
    }

    public boolean isDragingBarrle() {
        return  dragedPart == part.BARRLE;
    }
    public boolean isDragingBody() {
        return dragedPart == part.BODY;
    }
}